from selenium import webdriver
import unittest
import time
from selenium.webdriver.common.keys import Keys


class WebDriver(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome("C:\\Users\\viktor.zhulii\\Desktop\\PyTest\\chromedriver.exe")
        self.driver.get('https://www.youtube.com/?gl=UA')

    def test_01(self):
        driver = self.driver
#        driver.find_element_by_css_selector('[id="guide-button"]').click()
        input_icon = driver.find_element_by_css_selector('[id="search"][name="search_query"]')
        input_icon.send_keys("Marmok" + Keys.ENTER)
#        input_icon.send_keys(Keys.ENTER)
        time.sleep(2)

    def tearDown(self):
        self.driver.quit()


class GuruDriver(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome("C:\\Users\\viktor.zhulii\\Desktop\\PyTest\\chromedriver.exe")
        self.driver.get('https://www.guru99.com/api-testing.html')

    def test_02(self):
        driver = self.driver
        input_icon = driver.find_element_by_id("search")
        input_icon.send_keys("python")
        input_icon.send_keys(Keys.ENTER)
        time.sleep(2)

    def tearDown(self):
        self.driver.quit()


class Translation(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome("C:\\Users\\viktor.zhulii\\Desktop\\PyTest\\chromedriver.exe")
        self.driver.get("https://translate.google.com/?hl=uk")

    def test_03(self):
        driver = self.driver
        input_icon = driver.find_element_by_id("source")
        input_icon.send_keys("translation")
        input_icon.send_keys(Keys.ENTER)
        time.sleep(2)

    def tearDown(self):
        self.driver.quit()
