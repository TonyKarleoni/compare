import json
import requests


from API.data.Robotwars import *


def test_local_get_all():
    url = local_api_url1

    headers = ""
    payload = ""

    response = requests.request("GET", url, data=payload, headers=headers)

    print(response.text)

    assert response is not None
    assert response.status_code == 200


def test_local_get():
    url = local_api_url + "Mike"

    headers = ""
    payload = ""

    response = requests.request("GET", url, data=payload, headers=headers)

    print(response.text)

    assert response is not None
    assert response.status_code == 200


def test_local_post():
    url = local_api_url + "Lizz"

    headers = ""
    payload = local_api_body

    response = requests.request("POST", url, data=payload, headers=headers)

    print(response.text)

    assert response is not None
    assert response.status_code == 201


def test_local_delete():
    url = local_api_url + "Lizz"

    headers = ""
    payload = json.dumps(local_api_body)

    response = requests.request("DELETE", url, data=payload, headers=headers)

    print(response.text)

    assert response is not None
    assert response.status_code == 200
