import json

import requests


from API.data.Robotwars import robot_url, Robotwars_body, Robotwars_headers, Robotwars_body_no_question, api_url, \
    api_body


def test_apiblueprint_get():
    url = api_url

    headers = Robotwars_headers

    response = requests.request("GET", url, headers=headers)

    print(response.text)

    assert response is not None
    assert response.status_code == 200


def test_apiblueprint_post():
    url = api_url

    headers = Robotwars_headers
    payload = json.dumps(api_body)

    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.text)

    assert response is not None
    assert response.status_code == 200


def test_robot1_positive_case():
    url = robot_url

    payload = json.dumps(Robotwars_body)
    headers = Robotwars_headers

    response = requests.request("POST", url, data=payload, headers=headers)

    print(response.text)

    assert response is not None
    assert response.status_code == 400


def test_robot1_negative_case():
    url = robot_url

    payload = Robotwars_body_no_question
    headers = Robotwars_headers

    response = requests.request("POST", url, data=payload, headers=headers)

    print(response.text)

    assert response is not None
    assert response.status_code == 400



