robot_url = "http://polls.apiblueprint.org/questions"
api_url = "http://polls.apiblueprint.org/questions"
local_api_url = "http://127.0.0.1:5000/user/"
local_api_url1 = "http://127.0.0.1:5000/users/"


Robotwars_headers = {
    'Content-Type': "application/json",
    'cache-control': "no_cache",

}


Robotwars_body = {
    "productId": 319924,
    "crmId": "50000000075813279",
    "merchantReference": "test_445566",
    "startsOnStatus": "ON_DEMAND"
}


Robotwars_body_no_question = "{\n   \n    \"choices\": [\n        \"Swift\",\n        \"Python\",\n        \"Objective-C\",\n        \"Ruby\"\n    ]\n}"


api_body = {
    "question": "Favourite car - Audi A8",
    "choices": [
        "Swift",
        "Python",
        "Objective-C",
        "Ruby"
    ]
}


local_api_body = {
    "age": 24,
    "occupation": "tester"
}


'''parser json'''
#dict = response.json()
#contract_id = dict["contractId"[]]

#dict_token = response.json()
#Token = dict_token["token"]